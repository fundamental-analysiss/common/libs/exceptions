package com.naga.share.market.fundamental.analysis.exception;

import java.util.List;

import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.exceptions.ExceptionModel;

/**
 * @author Nagaaswin S
 *
 */
public class InternalException extends ParentExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InternalException(String exceptionCode) {
		super(exceptionCode);
	}

	public InternalException(String exceptionCode, String exceptionDescription) {
		super(exceptionCode, exceptionDescription);
	}

	public InternalException(String exceptionCode, String exceptionDescription, String exceptionType) {
		super(exceptionCode, exceptionDescription, exceptionType);
	}

	public InternalException(String exceptionCode, List<ExceptionModel> subExceptions) {
		super(exceptionCode, subExceptions);
	}

	public InternalException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions) {
		super(exceptionCode, exceptionType, subExceptions);
	}

	public InternalException(String exceptionCode, Throwable throwable) {
		super(exceptionCode, throwable);
	}

	public InternalException(String exceptionCode, String exceptionDescription, Throwable throwable) {
		super(exceptionCode, exceptionDescription, throwable);
	}

	public InternalException(String exceptionCode, String exceptionDescription, String exceptionType,
			Throwable throwable) {
		super(exceptionCode, exceptionDescription, exceptionType, throwable);
	}

	public InternalException(String exceptionCode, List<ExceptionModel> subExceptions, Throwable throwable) {
		super(exceptionCode, subExceptions, throwable);
	}

	public InternalException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions,
			Throwable throwable) {
		super(exceptionCode, exceptionType, subExceptions, throwable);
	}

	public InternalException(ExceptionModel excModel) {
		super(excModel);
	}

	@Override
	public int getReturnCode() {
		return ExceptionsConstants.INTERNAL_SYSTEM_ERROR_CODE;
	}

}
