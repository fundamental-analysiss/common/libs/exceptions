package com.naga.share.market.fundamental.analysis.exception;

import java.util.List;

import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.exceptions.ExceptionModel;

/**
 * @author Nagaaswin S
 *
 */
public class NoDataException extends ParentExceptions {

	private static final long serialVersionUID = 1L;

	public NoDataException(String exceptionCode) {
		super(exceptionCode);
	}

	public NoDataException(String exceptionCode, String exceptionDescription) {
		super(exceptionCode, exceptionDescription);
	}

	public NoDataException(String exceptionCode, String exceptionDescription, String exceptionType) {
		super(exceptionCode, exceptionDescription, exceptionType);
	}

	public NoDataException(String exceptionCode, List<ExceptionModel> subExceptions) {
		super(exceptionCode, subExceptions);
	}

	public NoDataException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions) {
		super(exceptionCode, exceptionType, subExceptions);
	}

	public NoDataException(String exceptionCode, Throwable throwable) {
		super(exceptionCode, throwable);
	}

	public NoDataException(String exceptionCode, String exceptionDescription, Throwable throwable) {
		super(exceptionCode, exceptionDescription, throwable);
	}

	public NoDataException(String exceptionCode, String exceptionDescription, String exceptionType,
			Throwable throwable) {
		super(exceptionCode, exceptionDescription, exceptionType, throwable);
	}

	public NoDataException(String exceptionCode, List<ExceptionModel> subExceptions, Throwable throwable) {
		super(exceptionCode, subExceptions, throwable);
	}

	public NoDataException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions,
			Throwable throwable) {
		super(exceptionCode, exceptionType, subExceptions, throwable);
	}

	public NoDataException(ExceptionModel excModel) {
		super(excModel);
	}

	@Override
	public int getReturnCode() {
		return ExceptionsConstants.NO_DATA_ERROR_CODE;
	}

}
