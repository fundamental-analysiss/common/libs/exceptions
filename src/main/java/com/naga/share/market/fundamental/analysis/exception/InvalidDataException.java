package com.naga.share.market.fundamental.analysis.exception;

import java.util.List;

import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.exceptions.ExceptionModel;

/**
 * @author Nagaaswin S
 *
 */
public class InvalidDataException extends ParentExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidDataException(String exceptionCode) {
		super(exceptionCode);
	}

	public InvalidDataException(String exceptionCode, String exceptionDescription) {
		super(exceptionCode, exceptionDescription);
	}

	public InvalidDataException(String exceptionCode, String exceptionDescription, String exceptionType) {
		super(exceptionCode, exceptionDescription, exceptionType);
	}

	public InvalidDataException(String exceptionCode, List<ExceptionModel> subExceptions) {
		super(exceptionCode, subExceptions);
	}

	public InvalidDataException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions) {
		super(exceptionCode, exceptionType, subExceptions);
	}

	public InvalidDataException(String exceptionCode, Throwable throwable) {
		super(exceptionCode, throwable);
	}

	public InvalidDataException(String exceptionCode, String exceptionDescription, Throwable throwable) {
		super(exceptionCode, exceptionDescription, throwable);
	}

	public InvalidDataException(String exceptionCode, String exceptionDescription, String exceptionType,
			Throwable throwable) {
		super(exceptionCode, exceptionDescription, exceptionType, throwable);
	}

	public InvalidDataException(String exceptionCode, List<ExceptionModel> subExceptions, Throwable throwable) {
		super(exceptionCode, subExceptions, throwable);
	}

	public InvalidDataException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions,
			Throwable throwable) {
		super(exceptionCode, exceptionType, subExceptions, throwable);
	}

	public InvalidDataException(ExceptionModel excModel) {
		super(excModel);
	}

	@Override
	public int getReturnCode() {
		return ExceptionsConstants.INVALID_DATA_ERROR_CODE;
	}

}
