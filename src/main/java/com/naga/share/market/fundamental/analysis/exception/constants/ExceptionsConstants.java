package com.naga.share.market.fundamental.analysis.exception.constants;

/**
 * @author Nagaaswin S
 *
 */
public class ExceptionsConstants {

	private ExceptionsConstants() {
	}

//	#Exception messages
	public static final String INVALID_DATA_ERROR = "Invalid data error";
	public static final String NO_DATA_ERROR = "No data error";
	public static final String MAPPING_DATA_ERROR = "Data mapping error";
	public static final String INTERNAL_SYSTEM_ERROR = "Internal system error";

//	#Exception codes
	public static final int INVALID_DATA_ERROR_CODE = 400;
	public static final int NO_DATA_ERROR_CODE = 400;
	public static final int DATA_MAPPING_ERROR_CODE = 403;
	public static final int INTERNAL_SYSTEM_ERROR_CODE = 500;
}
