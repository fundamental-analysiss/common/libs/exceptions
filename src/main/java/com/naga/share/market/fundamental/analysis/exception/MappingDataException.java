package com.naga.share.market.fundamental.analysis.exception;

import java.util.List;

import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.exceptions.ExceptionModel;

/**
 * @author Nagaaswin S
 *
 */
public class MappingDataException extends ParentExceptions {

	private static final long serialVersionUID = 1L;

	public MappingDataException(String exceptionCode) {
		super(exceptionCode);
	}

	public MappingDataException(String exceptionCode, String exceptionDescription) {
		super(exceptionCode, exceptionDescription);
	}

	public MappingDataException(String exceptionCode, String exceptionDescription, String exceptionType) {
		super(exceptionCode, exceptionDescription, exceptionType);
	}

	public MappingDataException(String exceptionCode, List<ExceptionModel> subExceptions) {
		super(exceptionCode, subExceptions);
	}

	public MappingDataException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions) {
		super(exceptionCode, exceptionType, subExceptions);
	}

	public MappingDataException(String exceptionCode, Throwable throwable) {
		super(exceptionCode, throwable);
	}

	public MappingDataException(String exceptionCode, String exceptionDescription, Throwable throwable) {
		super(exceptionCode, exceptionDescription, throwable);
	}

	public MappingDataException(String exceptionCode, String exceptionDescription, String exceptionType,
			Throwable throwable) {
		super(exceptionCode, exceptionDescription, exceptionType, throwable);
	}

	public MappingDataException(String exceptionCode, List<ExceptionModel> subExceptions, Throwable throwable) {
		super(exceptionCode, subExceptions, throwable);
	}

	public MappingDataException(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions,
			Throwable throwable) {
		super(exceptionCode, exceptionType, subExceptions, throwable);
	}

	public MappingDataException(ExceptionModel excModel) {
		super(excModel);
	}

	@Override
	public int getReturnCode() {
		return ExceptionsConstants.DATA_MAPPING_ERROR_CODE;
	}

}
