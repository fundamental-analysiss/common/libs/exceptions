package com.naga.share.market.fundamental.analysis.exception;

import java.util.List;

import com.naga.share.market.fundamental.analysis.model.exceptions.ExceptionModel;

/**
 * @author Nagaaswin S
 *
 */
public abstract class ParentExceptions extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ExceptionModel exceptionModel;

	public ParentExceptions(String exceptionCode) {
		this.exceptionModel = new ExceptionModel(exceptionCode);
	}

	public ParentExceptions(String exceptionCode, String exceptionDescription) {
		this.exceptionModel = new ExceptionModel(exceptionCode, exceptionDescription);
	}

	public ParentExceptions(String exceptionCode, String exceptionDescription, String exceptionType) {
		this.exceptionModel = new ExceptionModel(exceptionCode, exceptionDescription, exceptionType);
	}

	public ParentExceptions(String exceptionCode, List<ExceptionModel> subExceptions) {
		this.exceptionModel = new ExceptionModel(exceptionCode, subExceptions);
	}

	public ParentExceptions(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions) {
		this.exceptionModel = new ExceptionModel(exceptionCode, exceptionType, subExceptions);
	}

	public ParentExceptions(String exceptionCode, Throwable throwable) {
		super(throwable);
		this.exceptionModel = new ExceptionModel(exceptionCode);
	}

	public ParentExceptions(String exceptionCode, String exceptionDescription, Throwable throwable) {
		super(throwable);
		this.exceptionModel = new ExceptionModel(exceptionCode, exceptionDescription);
	}

	public ParentExceptions(String exceptionCode, String exceptionDescription, String exceptionType,
			Throwable throwable) {
		super(throwable);
		this.exceptionModel = new ExceptionModel(exceptionCode, exceptionDescription, exceptionType);
	}

	public ParentExceptions(String exceptionCode, List<ExceptionModel> subExceptions, Throwable throwable) {
		super(throwable);
		this.exceptionModel = new ExceptionModel(exceptionCode, subExceptions);
	}

	public ParentExceptions(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions,
			Throwable throwable) {
		super(throwable);
		this.exceptionModel = new ExceptionModel(exceptionCode, exceptionType, subExceptions);
	}

	public ParentExceptions(ExceptionModel excModel) {
		this.exceptionModel = excModel;
	}

	public ExceptionModel getExceptionModel() {
		return exceptionModel;
	}

	public abstract int getReturnCode();
}
